# Menus Everywhere! `:h menus`

This isn't *strictly* a gvim thing, but vim does give you the ability to make your very own menus. They work *better* in gvim than they do in terminal vim, but they *can* work in both places. Be aware, though, that this is a less painless topic than just about anything I have covered in this book. 

A> ## Why would I want menus?
A>
A> It's a good question. Vim is supposed to be the last word in *touch editing*, everything done via keyboard, nothing done via the mouse, right?
A>
A> And that's not a terrible goal. But here's the thing: there are a *lot* of commands to remember, and some of us could use a little break every once in a while, a little help with those awesome commands we only use every once in a while. Also, how cool do you feel when you have your own custom menu pop up whenever you want it?
A> 
A> The bottom line is this: If you don't want menus that's totally fine. But if there is a set of commands that you would like to be able to use without memorizing them that's fine too. Gvim's menu system has you covered.

First off, gvim loads a standard set of menus when you start it. You don't need to mess with them. You can if you want to; but you're on your own there. I'm going to cover the basics of creating a brand new menu, and then show you how to make it pop up when you want it, wherever you want it. 

There are a lot of caveats in the menu stuff: parts of it work cross platform, parts don't, so we're going to cover a basic menu with just a few options. This menu will provide access to Tim Pope's [fugitive](https://github.com/tpope/vim-fugitive) plugin, letting you run git commands without leaving vim. If you want to follow along *exactly* you'll need to install that plugin first. Checkout the chapter on [plugins](#plugins) for a quick refresher on how to add stuff using Pathogen.


Okay! Let's get started. Fugitive gives you access to a plethora of useful git features with parameters and ranges and powerful, intuitive features that we're going to ignore completely. For now we're going to provide access to exactly three features:

- Git Status
- Git Blame
- Git Remove

Which, now that we look at it, would make a pretty good layout for our menu! 

W> ##Make Sure You Test on a Git-Controlled File!
W>
W> When you're testing your menu setup you need to make sure the file you have open in your editor is under git version control, or all the commands will fail.

Fugitive's `Gstatus` command gives you access to a surprisingly wide array of functions; things like staging or un-staging files, and committing your changes. So naturally it would be the first thing we would want to add to our menu. So hey, I'll start there.

## Adding a Menu

Adding a menu is similar to adding a keymap: you can set the menu to only work in certain modes, or all modes. You can make it recursive or non-recursive, etc. etc. And, like keymapping, you can test your new menus *before* adding them to your `.gvimrc` file. Let's test it now: Open a file that is under git control and enter the following:

	:menu Git.Status :Gstatus<CR>	
And then hit `<enter>`. You should see a new "Git" menu item appear in your menu bar. If you click it you'll see a "Status" menu item. Click *that* and you should see the awesome fugitive status screen pop up in a new window. Nice, right? In this example we just used the `:menu` option, which enables your menu in Normal, Visual and Operator-pending mode. There are other options, which leads to a familiar looking table:

{title="Menu Commands"}
| Command	| Modes	|  
|  -------------------	| ------------|  
| `:menu`		| Normal, Insert, Operator-Pending |  
| `:nmenu`	| Normal	|  
| `:vmenu`		| Visual	|  
| `:omenu`	| Operator-pending  |  
| `:menu!`		| Insert, Command-line |  
| `:menu`		| Command-line	|  
| `:amenu`		| All 	|  

In this table I kinda saved the best for last there. Most of the time you'll probably use the `:amenu` option. In fact, there are two shortcuts you can use if you're just testing menus you haven't put into  your config files yet! The commands `:an` and `:am` are both aliases for `:amenu`[^Typo].

[^Typo]: I'm not sure why `:an` is in there. I like to believe that someone just kept hitting the wrong key and finally decided to map the command so that it would work.

Okay! Let's get back to the menu definition we set up earlier. After you choose where you want the menu to be active you name the menu. You'll notice that we called this one `Git.Status`. The period in the middle defines a *sub* menu, and you can keep going, if you hate your life and want to keep on nesting:

	:menu Git.Should.Be.Easier.To.Use.Than.This.Status :Gstatus<CR>

And you get something a little like this:

![Please, please don't do this](images/bad_menu.jpg)

But now that we've had our fun we're going to go back to *sane* menus, hmm?

## Keeping Things In Order

Unless you're some kind of crazy bohemian, you're going to want to define the order in which your menus appear. You do this by adding a *priority* to the menu definition. The lower the priority number, the closer to the left the menu will appear. The default menus have the following priorities[^priorities]

[^priorities]: you can see this list in the help text by using `:h menu-priority`

- File: 10
- Edit: 20
- Tools: 40
- Syntax: 50
- Buffers: 60
- Window: 70
- Help: 9999


In general they suggest *not* setting the priority of your menu higher than 9999, but nobody's stopping you. To create a menu with a priority you just put the priority before the menu command, like so:

	:80menu Git.Status :Gstatus<CR>

Which will put your menu after the Window menu. If you don't give a priority vim will assign your menu a priority of 500. If you want to set priorities for sub menus you put the priorities in a dot-seperated list *after* the menu command:

	:menu 80.100 Git.Status :Gstatus<CR>

Will put the **Git** menu after window, and set the **Status** menu fairly early on the list (because any menu without a set priority will default to 500).

Okay, so, armed with this information, let's create the menu for our thee git commands. The following would go well in your `.gvimrc` file:

	:menu 80.1 Git.Status :Gstatus<CR>
	:menu 80.2 Git.Blame :Gblame<CR>
	:menu 80.4 Git.Remove :Gremove<CR>

Save your file, source it, and you should have a pretty git menu all ready to go.

## Making the Menu Come to *You*

One of the whole points of vim is that you don't have to move your hands from the keyboard to do things. Which makes menus a problem. You have to pull one hand off of the keyboard, move the mouse *all the way to the top of the screen* to click on some stuff, and then get your hands back into position. Well, we're going to get rid of one of those steps.

Vim provides a `:popup` command that lets you call a specific menu to the position of your cursor. So, let's say you want your new git menu. Just go to the command line and type

	:popup Git

And the Git menu should show up...somewhere close to your mouse. The behavior of the popup option isn't *totally* reliable, and it may go away at some point, but for now it looks super awesome. It looks even cooler if you map a shortcut, like so:

	nmap <Leader>g :popup Git<CR>

So, armed with this knowledge, we can move on. The problem is that there are two divergent paths from here. Vim on Linux or Windows works pretty much the same, while MacVim has some of its own ideas about life. Choose the section that works best for you.

## Linux and Windows

If you look at the `File` menu on gvim under Linux or Windows you'll notice that some menu items have underlined letters, and even shortcuts listed to the side of the menu item. You're probably familiar with these from, well, *every other* program. If you press `<alt>o` when the File menu is open it'll open a new file. You can define both *accelerator keys* and *reminders* for your menus as well. All it takes is a few little additions to the menu command. Let's go back to our `Git.Status` entry. If we want to define `<alt>g` as the command to open the Git menu, we would set it up with an ampersand (&) before the letter G:

	:menu 80.1 &Git.&Status :Gstatus<CR>

We also set `<alt>s` to be the accelerator key for the status menu item. But let's say we want to get off of menus altogether. We can have the menu tell us the command we could run if we wanted to avoid the menu. You just put the text you want right-aligned after the literal string `<Tab>`, like so:

	:menu 80.1 &Git.&Status<Tab>:Gstatus :Gstatus<CR>

### Hidden Menus

If you want to be super cool you can set up a menu that won't appear in the menu bar, a menu that is only accessible as a popup. Again, this is a simple change: the name of your menu needs to have the character `]` at the beginning of its name, like so:

	:menu ]Git.Status<Tab>:Gstatus :Gstatus<CR>

And once you've defined it this way you need to remember to *call* it with the square bracket:

	nmap <Leader>g :popup ]Git<CR>

And viola! You've got a menu that appears as if by magic from nowhere! This, my friend, is the good life.

## MacVim is a Special Snowflake!

MacVim handles menus differently, which shouldn't be surprising, because OSX handles menus differently. If you're setting up a `.gvimrc` that is expected to be used in MacVim you've got two new commands to play with to make your menus fully Mac compatible.

However they're definitely *not* painless. As in "so far I haven't been able to add a new menu item using the MacVim menu commands." You can use the regular menu commands listed before, but assigning mac-specific keyboard shortcuts so far eludes me. And as I already mentioned, you can't hide menus in MacVim; they'll always show up on the menubar, even if you preface the name with `]`. So, if you want to learn how to make OSX specific menus check out `:h macvim-menus` from MacVim and let me know what you find.



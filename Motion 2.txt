# Moving Around in Vim: Grown Up Steps {#motion2}

Okay, now that the *serious business* of getting a color scheme set up is taken care of, let's get back to moving around. 

It's often said that programming is roughly 90% thinking and 10% typing [^programmers]. While this may be true, I would add that a fair amount of time is also spent getting the cursor where you need it to be. Let's say you made a change on line 15, and from there you need to move the end of the file to add a new method. In a regular editor you have a few choices: you can either press the down arrow a bunch of times, or take one hand off the keyboard entirely and click on the last line of the file. Vim has a better idea:

	G

That's it! Upper-case `G`, all by itself, will move your cursor to the last line of the file. Want to get back to the top of the file? 

	gg

 And you're there. The point is that moving around is supremely easy in vim, once you get the hang of it. So, let's take a look at some of the most used *motions*[^motions] in vim:

{title="simple motions"}
| Keystroke	| Motion			|  
|---------------|--------------------------------|
| h		| move right		|  
| j		| move down		|  
| k		| move up			|  
| l 		| move left			|  
|b		| previous word		|
|e		| end of word		|
| ctrl+d	| move down by 1/2 screen |  
| ctrl+f	| move down by 1 screen |  
|ctrl+u	| move up by 1/2  screen |  
|ctrl+b	| move up by 1 screen	| 
|gg		| jump to the start of the file |  
|G		| jump to the end of the file|  
|^		| first non-whitespace character of the line |  
|$		| last non-whitespace character of the line |  
|`0`		| beginning of the line 	| 

There are quite a few more, but this will give us enough to get on with. Let's play around with these actions for a moment. Open a file and jump around a bit, moving to the start and end of lines, bouncing around between words, etc. 

The "screen" motions are a little harder to pin down, but once you get used to them they make sense. A "screen" consists of the number of lines that fit in your current terminal window. So if your terminal window is 80 rows tall a screen is probably 79 rows (one row is taken up for the status line), and a half screen is around 40. But if you resize your terminal window the size of a "screen" changes accordingly.  Still, take some time getting used to "scrolling" through the file quickly using the 1/2 and full screen motions, and getting a feel for how fast you can get around. 

These motions are useful by themselves, and the better you get at them the more fun you'll have in vim. But they get even better when combined with the *operators* and *counts* we'll meet in the next two chapters. 


[^motions]: I use the name "motions" instead of "commands" or "actions" quite intentionally. For now don't worry about it, just remember it for later.

[^programmers]: By programmers at any rate. Managers would suggest that at least a portion of that 90% is *actually* spent furtively reading tech blogs and web comics.